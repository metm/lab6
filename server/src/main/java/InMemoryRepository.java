import domain.BaseEntity;
import domain.validators.IValidator;
import domain.validators.ValidatorException;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class InMemoryRepository<ID, T extends BaseEntity<ID>> implements Repository<ID,T> {

    private Map<ID, T> entities;
    private IValidator<T> validator;

    public InMemoryRepository(IValidator<T> validator) {
        this.validator = validator;
        entities = new HashMap<>();
    }

    @Override
    public Optional<T> add(T entity) throws ValidatorException {
        if (entity == null) {
            throw new IllegalArgumentException("ID must not be null");
        }
        validator.validate(entity);
        return Optional.ofNullable(entities.putIfAbsent(entity.getId(), entity));
    }

    @Override
    public Iterable<T> getAll() {
        return entities.entrySet().stream().map(entry -> entry.getValue()).collect(Collectors.toSet());
    }

    @Override
    public Optional<T> remove(ID id) throws IllegalArgumentException {
        if (id == null) {
            throw new IllegalArgumentException("ID must not be null");
        }
        return Optional.ofNullable(entities.remove(id));
    }

    @Override
    public Optional<T> update(T newMovie) throws ValidatorException {
        if(newMovie==null)
            throw new IllegalArgumentException("Entity must not be null");
        validator.validate(newMovie);
        return Optional.ofNullable(entities.computeIfPresent(newMovie.getId(), (k, v) -> newMovie));
    }
}
