import domain.Client;
import domain.Movie;
import domain.Rent;
import domain.validators.ClientValidator;
import domain.validators.IValidator;
import domain.validators.MovieValidator;
import domain.validators.RentValidator;
import repository.ClientDBRepo;
import repository.MovieDBRepo;
import repository.PagingRepository;
import repository.RentDBRepo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.UnaryOperator;

public class ServerApp {
    public static void main(String[] args){

        IValidator<Movie> movieValidator=new MovieValidator();
        IValidator<Client> clientValidator=new ClientValidator();
        IValidator<Rent> rentValidator = new RentValidator();

        PagingRepository<Integer, Client> clientRepo = new ClientDBRepo(clientValidator);
        PagingRepository<Integer, Movie> movieRepo = new MovieDBRepo(movieValidator);
        PagingRepository<Integer, Rent> rentRepo = new RentDBRepo(rentValidator);

        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        Service service = new ServerService(executorService, movieRepo, clientRepo,rentRepo);
        TcpServer tcpServer = new TcpServer(executorService, service.SERVER_PORT);

        handlers(tcpServer,service);
        tcpServer.startServer();
        System.out.println("bye - server");
    }

    public static UnaryOperator<Message> makeHandler(Message msg){
        for(Method m : Service.class.getDeclaredMethods())
            if(m.getName().equals(msg.getHeader()))
            {
                UnaryOperator<Message> handle = (request) -> {
                    try {
                        Future res = (Future<Message>) m.invoke(Service.class, msg.getBody());
                        return (Message)res.get();
                    } catch (IllegalAccessException | ExecutionException | InterruptedException | InvocationTargetException e) {
                        System.out.println(e.getMessage());
                    }
                    return null;
                };
                return handle;
            }
        return null;
    }

    public static void handlers(TcpServer tcpServer, Service service) {
        tcpServer.addHandler(Constants.AvailableMethods.addMovie.name(), (request) -> {
            Movie m = (Movie) request.getBody();
            Future res = service.addMovie(m);
            try {
                return (Message<String>) res.get();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            } catch (ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.getAllMovies.name(), (request) -> {
            Future res = service.getAllMovies();
            try {
                return (Message<Iterable<Movie>>)res.get();
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.removeMovie.name(), (request) -> {
            Integer id = (Integer) request.getBody();
            Future res = service.removeMovie(id);
            try{
                return (Message<String>)res.get();
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.updateMovie.name(), (request) -> {
            Movie m = (Movie) request.getBody();
            Future res = service.updateMovie(m);
            try {
                return (Message<String>)res.get();
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.addClient.name(), (request) -> {
            Client c = (Client) request.getBody();
            Future res = service.addClient(c);
            try {
                return (Message<String>) res.get();
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.getAllClients.name(), (request) -> {
            Future res = service.getAllClients();
            try {
                return (Message<Iterable<Client>>)res.get();
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.removeClient.name(), (request) -> {
            Integer id = (Integer) request.getBody();
            Future res = service.removeClient(id);
            try{
                return (Message<String>)res.get();
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.updateClient.name(), (request) -> {
            Client c = (Client) request.getBody();
            Future res = service.updateClient(c);
            try {
                return (Message<String>)res.get();
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.findMovie.name(), (request) -> {
            Integer id = (Integer) request.getBody();
            Future res = service.findMovie(id);
            try{
                return (Message<Movie>)res.get();
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.findClient.name(), (request) -> {
            Integer id = (Integer) request.getBody();
            Future res = service.findClient(id);
            try{
                return (Message<Client>)res.get();
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.addRent.name(), (request) -> {
            Rent r = (Rent) request.getBody();
            Future res = service.addRent(r);
            try {
                return (Message<String>) res.get();
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });

        tcpServer.addHandler(Constants.AvailableMethods.getAllRents.name(), (request) -> {
            Future res = service.getAllRents();
            try {
                return (Message<Iterable<Rent>>)res.get();
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.getMessage());
            }
            return null;
        });
    }
}
