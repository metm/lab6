import domain.BaseEntity;

import java.io.Serializable;

public interface PagingRepository<ID extends Serializable,
        T extends BaseEntity<ID>>
        extends CrudRepository<ID, T> {

    Page<T> findAll(Pageable pageable);
    

}
