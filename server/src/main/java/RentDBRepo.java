import domain.Rent;
import domain.validators.IValidator;
import domain.validators.ValidatorException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

//import java.util.Date;

public class RentDBRepo implements PagingRepository<Integer, Rent> {
    private static final String URL = "jdbc:postgresql://localhost:5432/MovieRentals";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "admin";
    private IValidator<Rent> validator;

    public RentDBRepo(IValidator<Rent> validator){
        this.validator = validator;
    }

    @Override
    public Page<Rent> findAll(Pageable pageable) {
        Iterable<Rent> rents = findAll();
        Stream<Rent> rentStream = StreamSupport.stream(rents.spliterator(), false)
                .skip(pageable.getPageNumber()*pageable.getPageSize()).limit(pageable.getPageSize());
        return new PageImpl<>(pageable, rentStream);
    }

    @Override
    public Optional<Rent> findOne(Integer optional) {
        return Optional.empty();
    }

    @Override
    public Iterable<Rent> findAll() {
        List<Rent> rents = new ArrayList<>();
        String sql = "select * from rentals";

        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                int id = resultSet.getInt("rentid");
                int movieID = resultSet.getInt("mid");
                int clientID = resultSet.getInt("cid");
                Date startDate = resultSet.getDate("startDate");
                Date dueDate = resultSet.getDate("dueDate");

                Rent rt = new Rent(movieID, clientID, startDate, dueDate);
                rt.setId(id);
                rents.add(rt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //return new HashSet<>(clients);
        return rents;
    }

    @Override
    public Optional<Rent> save(Rent entity) throws ValidatorException {
        String sql = "insert into rentals(rentid, mid,cid, startdate, duedate) values (?,?,?,?,?)";
        try(Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, entity.getId());
            statement.setInt(2, entity.getMovieID());
            statement.setInt(3, entity.getClientID());
            statement.setDate(4, entity.getStartDate());
            statement.setDate(5,entity.getDueDate());

            statement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Rent> delete(Integer id) {
        String sql = "delete from rentals where rentid=?";
        try(Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }

        return Optional.empty();
    }

    @Override
    public Optional<Rent> update(Rent entity) throws ValidatorException {
        String sql = "update rentals set mid=?, cid=?, startdate=?, duedate=? where rentid=?";
        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, entity.getMovieID());
            statement.setInt(2, entity.getClientID());
            statement.setDate(3, entity.getStartDate());
            statement.setDate(4,entity.getDueDate());
            statement.setInt(5, entity.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
