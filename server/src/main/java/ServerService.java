
import domain.Client;
import domain.Movie;
import domain.Rent;
import sun.security.validator.ValidatorException;


import java.security.Provider;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class ServerService implements Service {
    private ExecutorService executorService;
    private PagingRepository<Integer, Movie> movieRepository;
    private PagingRepository<Integer, Client> clientRepository;
    private PagingRepository<Integer, Rent> rentRepository;

    public ServerService(ExecutorService executorService,PagingRepository<Integer, Movie> movieRepository,PagingRepository<Integer, Client> clientRepository,PagingRepository<Integer, Rent> rentRepository){
        this.executorService=executorService;
        this.movieRepository=movieRepository;
        this.clientRepository=clientRepository;
        this.rentRepository=rentRepository;
    }

    public ServerService(ExecutorService executorService) {
        this.executorService = executorService;
    }

    @Override
    public Future<Message<String>> addMovie(Movie movie) {
        return executorService.submit(() -> {
            String msg;
            try {
                movieRepository.save(movie).get();
                msg = "There already exists a movie with the same ID";
            } catch (NoSuchElementException e){
                msg = "Movie added successfully";
            }
            return new Message<String>(Constants.AvailableMethods.addMovie.name(), msg);
        });
    }

    @Override
    public Future<Message<Iterable<Movie>>> getAllMovies() {
        return executorService.submit(() -> {
            return new Message<>(Constants.AvailableMethods.getAllMovies.name(), movieRepository.findAll());
        });
    }

    @Override
    public Future<Message<String>> removeMovie(Integer id) {
        return executorService.submit(() -> {
            String msg;
            /*try {
                movieRepository.delete(id).get();
                msg = "Movie removed successfully";
            }
            catch (NoSuchElementException e){
                msg = "No movie has the given ID";
            }*/
            movieRepository.delete(id);
            msg="The movie id is no longer existing!";
            return new Message<String>(Constants.AvailableMethods.removeMovie.name(), msg);
        });
    }

    @Override
    public Future<Message<String>> updateMovie(Movie newMovie) {
        return executorService.submit(() -> {
            String msg;
            try {
                movieRepository.update(newMovie).get();
                msg = "No movie has the given ID";
            }
            catch (NoSuchElementException e){
                msg = "Movie updated successfully";
            }
            return new Message<String>(Constants.AvailableMethods.updateMovie.name(), msg);
        });
    }

    @Override
    public Future<Message<Movie>> findMovie(Integer id) {
        return executorService.submit(() -> {
            return new Message<>(Constants.AvailableMethods.findMovie.name(), movieRepository.findOne(id).orElse(null));
        });
    }

    @Override
    public Future<Message<String>> addClient(Client client) {
        return executorService.submit(() -> {
            String msg;
            try {
                clientRepository.save(client).get();
                msg = "There already exists a client with the same ID";
            } catch (NoSuchElementException e){
                msg = "Client added successfully";
            }
            return new Message<String>(Constants.AvailableMethods.addClient.name(), msg);
        });
    }

    @Override
    public Future<Message<Iterable<Client>>> getAllClients() {
        return executorService.submit(() -> {
            return new Message<>(Constants.AvailableMethods.getAllClients.name(), clientRepository.findAll());
        });
    }

    @Override
    public Future<Message<String>> removeClient(Integer id) {
        return executorService.submit(() -> {
            String msg;
            clientRepository.delete(id);
            msg="The client id is no longer existing!";
            return new Message<String>(Constants.AvailableMethods.removeClient.name(), msg);
        });
    }

    @Override
    public Future<Message<String>> updateClient(Client newClient) {
        return executorService.submit(() -> {
            String msg;
            try {
                clientRepository.update(newClient).get();
                msg = "No client has the given ID";
            }
            catch (NoSuchElementException e){
                msg = "Client updated successfully";
            }
            return new Message<String>(Constants.AvailableMethods.updateClient.name(), msg);
        });
    }

    @Override
    public Future<Message<Client>> findClient(Integer id) {
        return executorService.submit(() -> {
            return new Message<>(Constants.AvailableMethods.findClient.name(), clientRepository.findOne(id).orElse(null));
        });
    }

    @Override
    public CompletableFuture<Message<String>> addRent(Rent rent) {
        return CompletableFuture.supplyAsync(() -> {
            String msg;
            try {
                rentRepository.save(rent).get();
                msg = "There already exists a rental with the same ID";
            } catch (NoSuchElementException e){
                msg = "Rental added successfully";
            }
            return new Message<String>(Constants.AvailableMethods.addRent.name(), msg);
        }, executorService);
    }

    @Override
    public Future<Message<Iterable<Rent>>> getAllRents() {
        return executorService.submit(() -> {
            return new Message<>(Constants.AvailableMethods.getAllRents.name(), rentRepository.findAll());
        });
    }

    @Override
    public Future<Message<Movie>> findMovieNameByID(Integer id) {
        return null;
    }
}
