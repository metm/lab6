import domain.Movie;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.function.UnaryOperator;

public class TcpServer {
    private ExecutorService executorService;
    private int serverPort;
    private Map<String, UnaryOperator<Message>> methodHandlers;

    public TcpServer(ExecutorService executorService, int serverPort) {
        this.executorService = executorService;
        this.serverPort = serverPort;
        this.methodHandlers = new HashMap<>();
    }

    public void addHandler(String methodName, UnaryOperator<Message> handler){
        methodHandlers.put(methodName, handler);
    }
    public void startServer(){
        try{
            ServerSocket serverSocket = new ServerSocket(serverPort);
            System.out.println("Server started - waiting for clients");
            while (true){
                Socket client = serverSocket.accept();
                System.out.println("client connected");
                executorService.submit(new ClientHandler(client));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class ClientHandler implements Runnable{
        private Socket socket;

        public ClientHandler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try(OutputStream os = socket.getOutputStream();
                InputStream is = socket.getInputStream();)
            {
                ObjectOutputStream oos = new ObjectOutputStream(os);
                ObjectInputStream ois = new ObjectInputStream(is);
                Message<Movie> request = new Message<>();
                request.readFrom(ois);
                System.out.println("server - received request: " + request);

                Message<String> response = methodHandlers.get(request.getHeader()).apply(request);
                System.out.println("server - computed response: " + response);
                response.writeTo(oos);
            }
            catch (IOException e){
                System.out.println(e.getMessage());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } finally {
                if(socket != null){
                    try{
                        socket.close();
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        }
    }
}
