package repository;

import domain.Client;
import domain.validators.IValidator;
import domain.validators.ValidatorException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class ClientDBRepo implements PagingRepository<Integer, Client> {
    private static final String URL = "jdbc:postgresql://localhost:5432/MovieRentals";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "admin";
    private IValidator<Client> validator;

    public ClientDBRepo(IValidator<Client> validator){
        this.validator = validator;
    }

    @Override
    public Page<Client> findAll(Pageable pageable) {
        Iterable<Client> clients = findAll();
        Stream<Client> clientStream = StreamSupport.stream(clients.spliterator(), false)
                .skip(pageable.getPageNumber()*pageable.getPageSize()).limit(pageable.getPageSize());
        return new PageImpl<>(pageable, clientStream);
    }

    @Override
    public Optional<Client> findOne(Integer ID) {

        String sql = "select * from clients where clientid=?";
        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, ID);
            try (ResultSet resultSet = statement.executeQuery()) {
                statement.setInt(1, ID);
                //Movie m = new Movie("", "", "");
                //m.setId(ID);
                while (resultSet.next()) {
                    String fName = resultSet.getString("firstName");
                    String lName = resultSet.getString("lastName");
                    //String genre = resultSet.getString("genre");
                    Client c = new Client(fName, lName);
                    c.setId(ID);
                    return Optional.of(c);
                }
                //return Optional.of(m);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return Optional.empty();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Iterable<Client> findAll() {
        List<Client> clients = new ArrayList<>();
        String sql = "select * from clients";

        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                int id = resultSet.getInt("clientID");
                String fname = resultSet.getString("firstName");
                String lname = resultSet.getString("lastName");

                Client cl = new Client(fname, lname);
                cl.setId(id);
                clients.add(cl);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //return new HashSet<>(clients);
        return clients;
    }

    @Override
    public Optional<Client> save(Client entity) throws ValidatorException {
        String sql = "insert into clients(clientid, firstName, lastName) values (?,?,?)";
        try(Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, entity.getId());
            statement.setString(2, entity.getfName());
            statement.setString(3,entity.getlName());

            statement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Client> delete(Integer id) {
        String sql = "delete from clients where clientID=?";
        try(Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }

        return Optional.empty();
    }

    @Override
    public Optional<Client> update(Client entity) throws ValidatorException {
        String sql = "update clients set firstName=?, lastName=? where clientID=?";
        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setString(1, entity.getfName());
            statement.setString(2, entity.getlName());
            statement.setInt(3, entity.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
