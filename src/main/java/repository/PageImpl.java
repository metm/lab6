package repository;

import java.util.stream.Stream;

public class PageImpl<T> implements Page<T> {
    private Pageable pageable;
    private Stream<T> content;

    public PageImpl(Pageable p, Stream<T> stream){
        this.pageable = p;
        this.content = stream;
    }
    @Override
    public Pageable getPageable() {
        return this.pageable;
    }

    @Override
    public Pageable nextPageable() {
        return new PageableImpl(this.pageable.getPageNumber() + 1, this.pageable.getPageSize());
    }

    @Override
    public Stream<T> getContent() {
        return this.content;
    }
}
