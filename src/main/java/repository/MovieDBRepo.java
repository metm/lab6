package repository;

import domain.Movie;
import domain.validators.IValidator;
import domain.validators.ValidatorException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class MovieDBRepo implements PagingRepository<Integer, Movie> {
    private static final String URL = "jdbc:postgresql://localhost:5432/MovieRentals";
    //private static final String USERNAME = System.getProperty("user");
    //private static final String PASSWORD = System.getProperty("password");
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "admin";
    private IValidator<Movie> validator;

    public MovieDBRepo(IValidator<Movie> validator){
        this.validator = validator;
    }

    @Override
    public Page<Movie> findAll(Pageable pageable) {
        Iterable<Movie> movies = findAll();
        Stream<Movie> clientStream = StreamSupport.stream(movies.spliterator(), false)
                .skip(pageable.getPageNumber()*pageable.getPageSize()).limit(pageable.getPageSize());
        return new PageImpl<>(pageable, clientStream);
    }

    @Override
    public Optional<Movie> findOne(Integer ID) {
        String sql = "select * from movies where movieid=?";
        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, ID);
            try (ResultSet resultSet = statement.executeQuery()) {

                //.executeUpdate();
                //Movie m = new Movie("", "", "");
                //m.setId(ID);
                while (resultSet.next()) {
                    String title = resultSet.getString("movieName");
                    String descr = resultSet.getString("description");
                    String genre = resultSet.getString("genre");
                    Movie m = new Movie(title, descr, genre);
                    m.setId(ID);
                    return Optional.of(m);
                }
                //return Optional.of(m);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return Optional.empty();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

        @Override
    public Iterable<Movie> findAll() {
        List<Movie> movies = new ArrayList<>();
        String sql = "select * from movies";

        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                int id = resultSet.getInt("movieID");
                String title = resultSet.getString("movieName");
                String descr = resultSet.getString("description");
                String genre = resultSet.getString("genre");

                Movie mv = new Movie(title, descr, genre);
                mv.setId(id);
                movies.add(mv);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //return new HashSet<>(clients);
        return movies;
    }

    @Override
    public Optional<Movie> save(Movie entity) throws ValidatorException {
        String sql = "insert into movies(movieid,moviename, description, genre) values (?,?,?,?)";
        try(Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, entity.getId());
            statement.setString(2, entity.getMovieName());
            statement.setString(3,entity.getMovieDescription());
            statement.setString(4,entity.getMovieGenre());

            statement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Movie> delete(Integer id) {
        String sql = "delete from movies where movieid=?";
        try(Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }

        return Optional.empty();
    }

    @Override
    public Optional<Movie> update(Movie entity) throws ValidatorException {
        String sql = "update movies set moviename=?, description=?, genre=? where movieid=?";
        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setString(1, entity.getMovieName());
            statement.setString(2,entity.getMovieDescription());
            statement.setString(3,entity.getMovieGenre());
            statement.setInt(4, entity.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
