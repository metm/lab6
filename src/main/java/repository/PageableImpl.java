package repository;

public class PageableImpl implements Pageable {
    private int pageNumber;
    private int pageSize;

    public PageableImpl(int nr, int size){
        this.pageNumber = nr;
        this.pageSize = size;
    }
    @Override
    public int getPageNumber() {
        return this.pageNumber;
    }

    @Override
    public int getPageSize() {
        return this.pageSize;
    }
}
