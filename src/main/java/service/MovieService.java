package service;

import domain.Movie;
import domain.validators.ValidatorException;
import repository.PageableImpl;
import repository.PagingRepository;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author Patricia
 */

public class MovieService {
    private PagingRepository<Integer, Movie> repository;
    private int page = 0;
    private int size = 1;

    public MovieService(PagingRepository<Integer, Movie> repository){
        this.repository = repository;
    }

    public void addMovie(Movie movie) throws ValidatorException {
        repository.save(movie);
    }

    public Set<Movie> getAllMovies(){
        Iterable<Movie> movies = repository.findAll();
        return StreamSupport.stream(movies.spliterator(), false).collect(Collectors.toSet());
    }

    public void removeMovie(Integer id) throws IllegalArgumentException{
        repository.delete(id);
    }

    public void updateMovie(Movie newMovie) throws ValidatorException {
        repository.update(newMovie);
    }

    public Movie findOne(Integer ID){
        return repository.findOne(ID).get();
    }

    /**
     * Returns all students whose name contain the given string.
     *
     * @param s
     * @return
     */

    public Set<Movie> filterMoviesByGenre(String s) {
        Iterable<Movie> movies = repository.findAll();
        Set<Movie> filteredMovies= new HashSet<>();
        movies.forEach(filteredMovies::add);
        filteredMovies.removeIf(movie -> !movie.getMovieGenre().contains(s));

        return filteredMovies;
    }

    public void setPageSize(int size){
        this.size = size;
    }

    public Set<Movie> getNextMovies(){
        Set<Movie> mv = repository.findAll(new PageableImpl(page, size)).getContent().collect(Collectors.toSet());
        page += 1;

        return mv;
    }
}
