package service;

import domain.Client;
import domain.validators.ValidatorException;
import repository.PageableImpl;
import repository.PagingRepository;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author Diana
 */

public class ClientService {
    private PagingRepository<Integer, Client> repository;
    private int page = 0;
    private int size = 1;

    public ClientService(PagingRepository<Integer, Client> repository)
    {
        this.repository=repository;
    }

    public void addClient(Client client) throws ValidatorException {
        repository.save(client);
    }

    public Set<Client> getAllClients(){
        Iterable<Client> clients = repository.findAll();
        return StreamSupport.stream(clients.spliterator(), false).collect(Collectors.toSet());
    }

    public void removeClient(Integer id) throws IllegalArgumentException{
        repository.delete(id);
    }

    public void updateClient(Client newClient) throws ValidatorException {
        repository.update(newClient);
    }

    public Client findOne(Integer ID){
        return repository.findOne(ID).get();
    }

    public void setPageSize(int size){
        this.size = size;
    }

    public Set<Client> getNextClients(){
        Set<Client> cl = repository.findAll(new PageableImpl(page, size)).getContent().collect(Collectors.toSet());
        page += 1;

        return cl;
    }

}
