import domain.Client;
import domain.Movie;
import domain.Rent;
import domain.validators.ClientValidator;
import domain.validators.IValidator;
import domain.validators.MovieValidator;
import domain.validators.RentValidator;
import repository.ClientDBRepo;
import repository.MovieDBRepo;
import repository.PagingRepository;
import repository.RentDBRepo;
import service.ClientService;
import service.MovieService;
import service.RentService;
import ui.Console;

public class Main {
    public static void main(String[] args) {
        IValidator<Movie> movieValidator=new MovieValidator();
        IValidator<Client> clientValidator=new ClientValidator();
        IValidator<Rent> rentValidator = new RentValidator();

        PagingRepository<Integer, Client> paginatedClientRepo = new ClientDBRepo(clientValidator);
        PagingRepository<Integer, Movie> paginatedMovieRepo = new MovieDBRepo(movieValidator);
        PagingRepository<Integer, Rent> paginatedRentRepo = new RentDBRepo(rentValidator);

        MovieService movieService = new MovieService(paginatedMovieRepo);
        ClientService clientService=new ClientService(paginatedClientRepo);
        RentService rentService = new RentService(paginatedRentRepo, paginatedMovieRepo, paginatedClientRepo);

        Console ui = new Console(movieService,clientService, rentService);
        try {
            ui.runConsole();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
