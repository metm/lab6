package ui;

import domain.Client;
import domain.Movie;
import domain.Rent;
import domain.validators.ValidatorException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import service.ClientService;
import service.MovieService;
import service.RentService;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.Set;

/**
 * @author Patricia & Diana
 */

public class Console {
    private MovieService movieService;
    private ClientService clientService;
    private RentService rentService;

    public Console(MovieService movieService, ClientService clientService, RentService rentService){
        this.movieService = movieService;
        this.clientService=clientService;
        this.rentService=rentService;
    }

    public void runConsole() throws Exception {
        //addMovies();
        //printAllMovies();
        int command = -1;
        while (true){
            printMenu();
            Scanner keyboard = new Scanner(System.in);
            System.out.println("Enter the number of a command");
            command = keyboard.nextInt();
            switch (command){
                case 0: System.exit(0);
                case 1: addMovies(); break;
                case 2: printAllMovies(); break;
                case 3: removeMovies(); break;
                case 4: updateMovie(); break;
                case 5:filterMovies();break;
                case 6: addClients();break;
                case 7:printAllClients();break;
                case 8: removeClient();break;
                case 9: updateClient();break;
                case 10: addRent();break;
                case 11: printAllRents(); break;
                case 12:rentalStatistics();break;
                case 13:printClientsWithPaging();break;
                case 14:printMoviesWithPaging();break;
                case 15:printRentsWithPaging();break;
                default:
                    System.out.println("Choose a valid command please:)");
            }

        }
    }

    private void printMenu(){
        System.out.println("1.Add a movie.\n2.Display all movies.\n3.Remove a movie.\n4.Update movie.\n5.Filter movies by genre\n6.Add a client.\n7.Display all clients.\n8.Remove a client.\n9.Update client.\n10. Add rent.\n11. Display rentals\n12. Rental statistics.\n0.Exit");
    }

    private void printAllMovies(){
        Set<Movie> movies = movieService.getAllMovies();
        movies.forEach(System.out::println);
    }

    private void printAllClients()
    {
        Set<Client> clients=clientService.getAllClients();
        clients.forEach(System.out::println);
    }

    private void printAllRents()
    {
        Set<Rent> rentals=rentService.getAllRents();
        rentals.forEach(e->{
            System.out.println("Rent id: "+e.getId()+"\nMovie: "+movieService.findOne(e.getMovieID()).toString()
            + "\nClient: " + clientService.findOne(e.getClientID()).toString() + "\nValability: " + e.getStartDate() + "->" + e.getDueDate());
        });
    }

    private void updateFiles()
    {
        File file=new File("./data/movies");
        Path path= Paths.get("./data/movies");
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(file);
            writer.print("");
            Set<Movie> movies = movieService.getAllMovies();
            PrintWriter finalWriter = writer;
            movies.forEach(movie -> {
                finalWriter.print(movie.getId() + "," + movie.getMovieName() + "," + movie.getMovieDescription() + "," + movie.getMovieGenre()+"\n"); });
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        File cfile=new File("./data/clients");
        Path cpath= Paths.get("./data/clients");
        PrintWriter cwriter = null;
        try {
            cwriter = new PrintWriter(cfile);
            cwriter.print("");
            Set<Client> clients=clientService.getAllClients();
            PrintWriter cfinalWriter = cwriter;
            clients.forEach(client -> {
                cfinalWriter.print(client.getId() + "," + client.getfName() + "," + client.getlName() +"\n"); });
            cwriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveAll() throws Exception{
        Document document = DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder().newDocument();

        Set<Movie> movies = movieService.getAllMovies();
        Element root = document.createElement("movies");

        movies.forEach(movie -> {

            Element movieElement = document.createElement("movie");
            movieElement.setAttribute("id", String.valueOf(movie.getId()));
            root.appendChild(movieElement);
            appendChildWithText(document, movieElement, "name", movie.getMovieName());
            appendChildWithText(document, movieElement, "description", movie.getMovieDescription());
            appendChildWithText(document, movieElement, "genre", movie.getMovieGenre());

            Transformer transformer =
                    null;
            try {
                transformer = TransformerFactory.newInstance().newTransformer();
            } catch (TransformerConfigurationException e) {
                e.printStackTrace();
            }
            try {
                assert transformer != null;
                transformer.transform(new DOMSource(root),
                        new StreamResult(new FileOutputStream(
                                "./data/movies.xml")));
            } catch (TransformerException | FileNotFoundException e) {
                e.printStackTrace();
            }
        });

        Document document2 = DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder().newDocument();

        Set<Client> clients = clientService.getAllClients();
        Element root2 = document2.createElement("clients");
        clients.forEach(client -> {Element clientElement = document2.createElement("client");
            clientElement.setAttribute("id", String.valueOf(client.getId()));
            root2.appendChild(clientElement);

            appendChildWithText(document2, clientElement, "firstName", client.getfName());
            appendChildWithText(document2, clientElement, "lastName", client.getlName());

            Transformer transformer =
                    null;
            try {
                transformer = TransformerFactory.newInstance().newTransformer();
            } catch (TransformerConfigurationException e) {
                e.printStackTrace();
            }
            try {
                assert transformer != null;
                transformer.transform(new DOMSource(root2),
                        new StreamResult(new FileOutputStream(
                                "./data/clients.xml")));
            } catch (TransformerException | FileNotFoundException e) {
                e.printStackTrace();
            }
        });
    }

    private static void appendChildWithText(Document document,
                                            Node parent, String tagName, String textContent) {
        Element element = document.createElement(tagName);
        element.setTextContent(textContent);
        parent.appendChild(element);
    }


    private void addMovies(){
        //while (true){
            Movie movie = readMovie();
            if (!(movie == null || movie.getId() < 0)) {
                try {
                    movieService.addMovie(movie);
                } catch (ValidatorException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                System.out.println("Invalid id");
            }
    }

    private void addClients(){
        Client client=readClient();
        if(!(client==null || client.getId()<0)){
            try{
                clientService.addClient(client);
            }catch (ValidatorException e){
                e.printStackTrace();
            }
        }
        else
        {
            System.out.println("Invalid id");
        }
    }

    private Movie readMovie(){
        System.out.println("Read movie {id, name, description, genre}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try{
            Integer id = Integer.valueOf(bufferRead.readLine());
            String name = bufferRead.readLine();
            String description = bufferRead.readLine();
            String genre = bufferRead.readLine();

            Movie movie = new Movie(name, description, genre);
            movie.setId(id);

            return movie;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private Client readClient(){
        System.out.println("Read client {id, first name, last name}");

        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
        try {
            Integer id=Integer.valueOf(bufferedReader.readLine());
            String fname=bufferedReader.readLine();
            String lname=bufferedReader.readLine();
            Client client=new Client(fname,lname);
            client.setId(id);
            return client;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void removeMovies(){
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter the ID of the movie you want to remove");
        int id = keyboard.nextInt();
        if (id > 0)
            movieService.removeMovie(id);
        else
            System.out.println("Invalid ID");
    }

    private void removeClient(){
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter the ID of the client you want to remove");
        int id = keyboard.nextInt();
        if (id > 0)
            clientService.removeClient(id);
        else
            System.out.println("Invalid ID");
    }

    private void updateMovie(){
        System.out.println("Make a movie update {id, name, description, genre}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            Integer id = Integer.valueOf(bufferRead.readLine());
            String newName = bufferRead.readLine();
            String newDescription = bufferRead.readLine();
            String newGenre = bufferRead.readLine();
            Movie movie = new Movie(newName, newDescription, newGenre);
            movie.setId(id);
            if (!(movie.getId() < 0))
            {
                try {
                    movieService.updateMovie(movie);
                }
                catch (IllegalArgumentException e)
                {
                    System.out.println("Invalid movie");
                }
            }
            else {
                System.out.println("Invalid id");
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void updateClient(){
        System.out.println("Make a client update {id, first name, last name}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            Integer id = Integer.valueOf(bufferRead.readLine());
            String newFirstName = bufferRead.readLine();
            String newLastName = bufferRead.readLine();
            Client client=new Client(newFirstName,newLastName);
            client.setId(id);
            if (!(client.getId() < 0))
            {
                try {
                    clientService.updateClient(client);
                }
                catch (IllegalArgumentException e)
                {
                    System.out.println("Invalid client");
                }
            }
            else {
                System.out.println("Invalid id");
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void filterMovies() {
        System.out.println("Enter a genre: ");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            String genre = bufferRead.readLine();
            Set<Movie> movies = movieService.filterMoviesByGenre(genre);
            movies.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Rent rentMovie(){
        System.out.println("Read rent {id, movieID, clientID, startDate, dueDate}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try{
            Integer id = Integer.valueOf(bufferRead.readLine());
            Integer movieID = Integer.valueOf(bufferRead.readLine());
            Integer clientID = Integer.valueOf(bufferRead.readLine());
            String startDate = bufferRead.readLine();
            String dueDate = bufferRead.readLine();


            SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
            java.util.Date parsed = format.parse(startDate);
            Date stDate = new Date(parsed.getTime());

            SimpleDateFormat format1 = new SimpleDateFormat("ddMMyyyy");
            java.util.Date parsed1 = format1.parse(dueDate);
            Date dDate = new Date(parsed1.getTime());

            Rent rent = new Rent(movieID, clientID, stDate, dDate);
            rent.setId(id);
            //movie.setId(id);

            return rent;
        } catch (IOException | ParseException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private void addRent(){
        Rent rent = rentMovie();
        if(!(rent==null || rent.getId()<0)){
            try{
                rentService.addRent(rent);
            }catch (ValidatorException e){
                e.printStackTrace();
            }
        }
        else
        {
            System.out.println("Invalid id");
        }
    }

    private void rentalStatistics(){
        System.out.print("Most rented movie:\n");
        System.out.print(rentService.rentStatistics());
    }

    private void printClientsWithPaging() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter page size: ");
        int size = scanner.nextInt();
        clientService.setPageSize(size);

        System.out.println("enter 'n' - for next; 'x' - for exit: ");

        while (true) {
            String cmd = scanner.next();
            if (cmd.equals("x")) {
                System.out.println("exit");
                break;
            }
            if (!cmd.equals("n")) {
                System.out.println("this option is not yet implemented");
                continue;
            }

            Set<Client> clients = clientService.getNextClients();
            if (clients.size() == 0) {
                System.out.println("no more clients");
                break;
            }
            clients.forEach(System.out::println);
        }
    }

    private void printMoviesWithPaging() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter page size: ");
        int size = scanner.nextInt();
        movieService.setPageSize(size);

        System.out.println("enter 'n' - for next; 'x' - for exit: ");

        while (true) {
            String cmd = scanner.next();
            if (cmd.equals("x")) {
                System.out.println("exit");
                break;
            }
            if (!cmd.equals("n")) {
                System.out.println("this option is not yet implemented");
                continue;
            }

            Set<Movie> movies = movieService.getNextMovies();
            if (movies.size() == 0) {
                System.out.println("no more movies");
                break;
            }
            movies.forEach(System.out::println);
        }
    }

    private void printRentsWithPaging() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter page size: ");
        int size = scanner.nextInt();
        rentService.setPageSize(size);

        System.out.println("enter 'n' - for next; 'x' - for exit: ");

        while (true) {
            String cmd = scanner.next();
            if (cmd.equals("x")) {
                System.out.println("exit");
                break;
            }
            if (!cmd.equals("n")) {
                System.out.println("this option is not yet implemented");
                continue;
            }

            Set<Rent> rents = rentService.getNextRents();
            if (rents.size() == 0) {
                System.out.println("no more rentals");
                break;
            }
            rents.forEach(System.out::println);
        }
    }
}
