package domain.validators;

import domain.Movie;

/**
 * @author Diana
 */

public class MovieValidator implements IValidator<Movie> {

    @Override
    public void validate(Movie entity) throws ValidatorException {
        if(entity.getMovieName().equals(""))
            throw new ValidatorException("Null name");
        if(entity.getMovieDescription().equals(""))
            throw new ValidatorException("Null description");
        if(entity.getMovieGenre().equals(""))
            throw new ValidatorException("Null genre");
    }
}
