package domain.validators;

import domain.Rent;

public class RentValidator implements IValidator<Rent> {
    @Override
    public void validate(Rent entity) throws ValidatorException {
        if(entity.getClientID() < 0)
            throw new ValidatorException("Invalid ClientID");
        if(entity.getMovieID() < 0)
            throw new ValidatorException("Invalid MovieID");
    }
}
