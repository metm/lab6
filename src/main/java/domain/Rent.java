package domain;

import java.io.Serializable;
import java.sql.Date;

public class Rent extends BaseEntity<Integer> implements Serializable {
    private int movieID;
    private int clientID;
    private Date startDate;
    private Date dueDate;

    public Rent(int movieID, int clientID, Date startDate, Date dueDate){
        this.movieID = movieID;
        this.clientID = clientID;
        this.startDate = startDate;
        this.dueDate = dueDate;
    }


    public int getMovieID() {
        return movieID;
    }

    public void setMovieID(int movieID) {
        this.movieID = movieID;
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    @Override
    public String toString() {

        return "Rental " + super.toString() + "\nMovieID: " + movieID + "\nClientID: " + clientID + "\nValability: " + startDate + "->" + dueDate;
    }
}
