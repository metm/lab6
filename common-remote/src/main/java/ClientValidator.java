public class ClientValidator implements IValidator<Client> {
    @Override
    public void validate(Client entity) throws ValidatorException {
        if(entity.getfName().equals(""))
            throw new ValidatorException("Null first name");
        if(entity.getlName().equals(""))
            throw new ValidatorException("Null last name");
    }
}