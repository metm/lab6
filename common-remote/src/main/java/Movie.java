import java.io.Serializable;

/**
 * @author Diana
 */

public class Movie extends BaseEntity<Integer> implements Serializable {
    private String movieName;
    private String movieDescription;
    private String movieGenre;

    public Movie(String name,String description,String genre)
    {
        movieName=name;
        movieDescription=description;
        movieGenre=genre;
    }

    public String getMovieName() {
        return movieName;
    }

    public String getMovieDescription() {
        return movieDescription;
    }

    public String getMovieGenre() {
        return movieGenre;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public void setMovieDescription(String movieDescription) {
        this.movieDescription = movieDescription;
    }

    public void setMovieGenre(String movieGenre) {
        this.movieGenre = movieGenre;
    }

    @Override
    public String toString() {
        return  super.toString()+movieName +" , Description: "+ movieDescription+", Genre: " + movieGenre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        if (!movieDescription.equals(movie.movieDescription)) return false;
        if (!movieGenre.equals(movie.movieGenre)) return false;
        return movieName.equals(movie.movieName);
    }
}
