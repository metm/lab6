import java.io.Serializable;

/**
 * @author Diana
 */

public class Client extends BaseEntity<Integer> implements Serializable {
    private String fName;
    private String lName;

    public Client(String firstName,String lastName)
    {
        this.fName=firstName;
        this.lName=lastName;
    }


    public String getfName() {
        return fName;
    }

    public String getlName() {
        return lName;
    }


    public void setfName(String fName) {
        this.fName = fName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    @Override
    public String toString() {
        return super.toString()+ fName + " "+ lName;
    }
}
