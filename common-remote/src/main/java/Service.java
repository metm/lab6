import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public interface Service {
    String SERVER_HOST = "localhost";
    int SERVER_PORT =1234;

    String addMovie(Movie movie);
    Iterable<Movie> getAllMovies();
    String removeMovie(Integer id);
    String updateMovie(Movie newMovie);
    Movie findMovie(Integer id);

    String addClient(Client client);
    Iterable<Client> getAllClients();
    String removeClient(Integer id);
    String updateClient(Client newClient);
    Client findClient(Integer id);

    String addRent(Rent rent);
    Iterable<Rent> getAllRents();
    Movie findMovieNameByID(Integer id);
}
