import domain.Movie;
import domain.Client;
import domain.*;

import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public interface Service {
    String SERVER_HOST = "localhost";
    int SERVER_PORT =1234;

    Future<Message<String>> addMovie(Movie movie);
    Future<Message<Iterable<Movie>>> getAllMovies();
    Future<Message<String>> removeMovie(Integer id);
    Future<Message<String>> updateMovie(Movie newMovie);
    Future<Message<Movie>> findMovie(Integer id);

    Future<Message<String>> addClient(Client client);
    Future<Message<Iterable<Client>>> getAllClients();
    Future<Message<String>> removeClient(Integer id);
    Future<Message<String>> updateClient(Client newClient);
    Future<Message<Client>> findClient(Integer id);

    CompletableFuture<Message<String>> addRent(Rent rent);
    Future<Message<Iterable<Rent>>> getAllRents();
    Future<Message<Movie>> findMovieNameByID(Integer id);
}
