public final class Constants {
    public enum AvailableMethods{
        addMovie,
        getAllMovies,
        removeMovie,
        updateMovie,
        findMovie,
        
        addClient,
        getAllClients,
        removeClient,
        updateClient,
        findClient,
        
        addRent,
        getAllRents,
        findMovieNameByID
    }
}
