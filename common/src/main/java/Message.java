import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Message<T> implements Serializable {
    private String header;
    private T body;
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");

    public Message() {
    }

    public Message(String header) {
        this.header = header;
    }

    public Message(String header, T body) {
        this.header = header;
        this.body = body;
    }


    public String getHeader() {
        return header;
    }

    public T getBody() {
        return body;
    }

    @Override
    public String toString() {
        return "Message{" +
                "header='" + header + '\'' +
                ", body=" + body +
                '}';
    }

    public void writeTo(ObjectOutputStream oos) throws IOException {
        Message<T> msg = new Message<>(this.header,this.body);
        oos.writeObject(msg);
    }

    public void readFrom(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        Message<T> obj = (Message<T>)ois.readObject();
        header=obj.header;
        body = obj.body;
    }

    public static MessageBuilder builder() {
        return new MessageBuilder();
    }

    public static class MessageBuilder {
        private Message message;

        MessageBuilder() {
            message = new Message();
        }

        public MessageBuilder header(String header) {
            this.message.header = header;
            return this;
        }

        public MessageBuilder body(String body) {
            this.message.body = body;
            return this;
        }

        public Message build() {
            return this.message;
        }
    }
}
