import org.springframework.beans.factory.annotation.Autowired;

public class ServiceImplClient implements Service {
    @Autowired
    private Service service;

    @Override
    public String addMovie(Movie movie) {
        return service.addMovie(movie);
    }

    @Override
    public Iterable<Movie> getAllMovies() {
        return service.getAllMovies();
    }

    @Override
    public String removeMovie(Integer id) {
        return service.removeMovie(id);
    }

    @Override
    public String updateMovie(Movie newMovie) {
        return service.updateMovie(newMovie);
    }

    @Override
    public Movie findMovie(Integer id) {
        return service.findMovie(id);
    }

    @Override
    public String addClient(Client client) {
        return service.addClient(client);
    }

    @Override
    public Iterable<Client> getAllClients() {
        return service.getAllClients();
    }

    @Override
    public String removeClient(Integer id) {
        return service.removeClient(id);
    }

    @Override
    public String updateClient(Client newClient) {
        return service.updateClient(newClient);
    }

    @Override
    public Client findClient(Integer id) {
        return service.findClient(id);
    }

    @Override
    public String addRent(Rent rent) {
        return service.addRent(rent);
    }

    @Override
    public Iterable<Rent> getAllRents() {
        return service.getAllRents();
    }

    @Override
    public Movie findMovieNameByID(Integer id) {
        return service.findMovieNameByID(id);
    }
}
