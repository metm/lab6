import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ClientApp {
    public static void main(String[] args){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("client-remote.src.main.java");
        Service client = (Service) context.getBean("ServiceClient");
//        Service client = context.getBean(ServiceClient);

        Console console = new Console(client);
        console.runConsole();

        System.out.println("bye - client");
    }
}
