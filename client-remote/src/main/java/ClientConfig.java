import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

@Configuration
public class ClientConfig {
    @Bean
    Service ServiceClient(){return new ServiceImplClient() {
    };}

    @Bean
    RmiProxyFactoryBean rmiProxyFactoryBean(){
        RmiProxyFactoryBean proxy = new RmiProxyFactoryBean();
        proxy.setServiceInterface(Service.class);
        proxy.setServiceUrl("rmi://localhost:5432/MovieRentals");
        return proxy;
    }
}
