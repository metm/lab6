import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class ClientCRUDConsole {
    private Service clientService;
    private Scanner scanner;
    private ExecutorService executorService;

    public ClientCRUDConsole(Service clientService, Scanner scanner, ExecutorService executorService) {
        this.clientService = clientService;
        this.scanner = scanner;
        this.executorService = executorService;
    }

    public void runConsole() {
        String cmd;
        boolean ok=true;
        while(ok){
            menu();
            System.out.print("Enter a command: ");
            cmd = scanner.next();
            switch (cmd) {
                case "1":
                    addClient();
                    //this.executorService.submit(()->addMovie());
                    break;
                case "2":
                    printAllClients();
                    //this.executorService.submit(()->printAllMovies());
                    break;
                case "3":
                    deleteClient();
                    //this.executorService.submit(() -> deleteMovie());
                    break;
                case "4":
                    updateClient();
                    //this.executorService.submit(()->updateMovie());
                    break;
                case "0":
                    ok=false;
                    break;
                default:
                    break;
            }
        }
    }

//    private static <T> T unpack(T msg){
//        try {
//            return msg.get().getBody();
//        } catch (InterruptedException e) {
//            System.out.println("An InterruptedException occured");
//        } catch (ExecutionException e) {
//            System.out.println("An ExecutionException occured");
//        }
//        return null;
//    }

    private void printAllClients() {
        Iterable<Client> clients = clientService.getAllClients();
        if (clients == null) return;
        clients.forEach(System.out::println);
        // try {
        //   movieService.getAllMovies().get().getBody().forEach(System.out::println);
        //} catch (InterruptedException | ExecutionException e) {
        //  e.printStackTrace();
        //}
    }

    private Client readClient()
    {
        System.out.println("Read client {id, first name, last name}");

        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
        try {
            Integer id=Integer.valueOf(bufferedReader.readLine());
            String fname=bufferedReader.readLine();
            String lname=bufferedReader.readLine();
            Client client=new Client(fname,lname);
            client.setId(id);
            return client;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void addClient()
    {
        Client client=readClient();
        System.out.println(clientService.addClient(client));
        /*if(!(client==null || client.getId()<0)){
            try{
                clientService.addClient(client);
            }catch (ValidatorException e){
                e.printStackTrace();
            }
        }
        else
        {
            System.out.println("Invalid id");
        }*/
    }

    private void deleteClient() {
        System.out.println("Enter the ID of the client you want to remove");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        Integer id= null;
        try {
            id = Integer.valueOf(bufferRead.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(clientService.removeClient(id));
    }

    private void updateClient(){
        System.out.println("Make a client update {id, first name, last name}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            Integer id = Integer.valueOf(bufferRead.readLine());
            String newFirstName = bufferRead.readLine();
            String newLastName = bufferRead.readLine();
            Client client=new Client(newFirstName,newLastName);
            client.setId(id);
            if (!(client.getId() < 0))
            {
                try {
                    System.out.println(clientService.updateClient(client));
                }
                catch (IllegalArgumentException e)
                {
                    System.out.println("Invalid client");
                }
            }
            else {
                System.out.println("Invalid id");
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void menu(){
        System.out.println("1. Add a client.");
        System.out.println("2. Print all clients.");
        System.out.println("3. Delete a client.");
        System.out.println("4. Update a client.");
        System.out.println("0. Exit.");
    }
}
