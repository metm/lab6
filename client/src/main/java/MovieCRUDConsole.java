import domain.Movie;
import domain.validators.ValidatorException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class MovieCRUDConsole {
    private Service movieService;
    private Scanner scanner;
    private ExecutorService executorService;

    public MovieCRUDConsole(Service movieService, Scanner scanner, ExecutorService executorService) {
        this.movieService = movieService;
        this.scanner = scanner;
        this.executorService = executorService;
    }

    public void runConsole() {
        String cmd;
        boolean ok=true;
        while(ok){
            menu();
            System.out.print("Enter a command: ");
            cmd = scanner.next();
            switch (cmd) {
                case "1":
                    addMovie();
//                    this.executorService.submit(()->addMovie());
//                    try {
//                        executorService.awaitTermination(100, TimeUnit.MICROSECONDS);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                    break;
                case "2":
                    printAllMovies();
//                    this.executorService.submit(()->printAllMovies());
//                    try {
//                        executorService.awaitTermination(100, TimeUnit.MICROSECONDS);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                    break;
                case "3":
                    deleteMovie();
                    //this.executorService.submit(() -> deleteMovie());
                    break;
                case "4":
                    updateMovie();
                    //this.executorService.submit(()->updateMovie());
                    break;
                case "0":
                    ok=false;
                    break;
                default:
                    break;
            }
        }
    }

    private static <T> T unpack(Future<Message<T>> msg){
        try {
            return msg.get().getBody();
        } catch (InterruptedException e) {
            System.out.println("An InterruptedException occured");
        } catch (ExecutionException e) {
            System.out.println("An ExecutionException occured");
        }
        return null;
    }

    private void printAllMovies() {
        Iterable<Movie> movies = unpack(movieService.getAllMovies());
        if (movies == null) return;
        movies.forEach(System.out::println);
       // try {
         //   movieService.getAllMovies().get().getBody().forEach(System.out::println);
        //} catch (InterruptedException | ExecutionException e) {
          //  e.printStackTrace();
        //}
    }

    private void menu(){
        System.out.println("1. Add a movie.");
        System.out.println("2. Print all movies.");
        System.out.println("3. Delete a movie.");
        System.out.println("4. Update a movie.");
        System.out.println("0. Exit.");
    }

    private void updateMovie(){
        System.out.println("Make a movie update {id, name, description, genre}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            Integer id = Integer.valueOf(bufferRead.readLine());
            String newName = bufferRead.readLine();
            String newDescription = bufferRead.readLine();
            String newGenre = bufferRead.readLine();
            Movie movie = new Movie(newName, newDescription, newGenre);
            movie.setId(id);
            if (!(movie.getId() < 0))
            {
                try {
                    System.out.println(unpack(movieService.updateMovie(movie)));
                }
                catch (IllegalArgumentException e)
                {
                    System.out.println("Invalid movie");
                }
            }
            else {
                System.out.println("Invalid id");
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void deleteMovie() {
        System.out.println("Enter the ID of the movie you want to remove");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        Integer id= null;
        try {
            id = Integer.valueOf(bufferRead.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(unpack(movieService.removeMovie(id)));
    }

    private Movie readMovie(){
        System.out.println("Read movie {id, name, description, genre}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try{
            Integer id = Integer.valueOf(bufferRead.readLine());
            String name = bufferRead.readLine();
            String description = bufferRead.readLine();
            String genre = bufferRead.readLine();

            Movie movie = new Movie(name, description, genre);
            movie.setId(id);

            return movie;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private void addMovie() {
        Movie movie = readMovie();
        System.out.println(unpack(movieService.addMovie(movie)));
        /*if (!(movie == null || movie.getId() < 0)) {
            try {
                movieService.addMovie(movie);
            } catch (ValidatorException e) {
                e.printStackTrace();
            }
        }
        else
        {
            System.out.println("Invalid id");
        }*/
    }


}
