import java.util.Scanner;
import java.util.concurrent.ExecutorService;

public class Console {
    private Service service;
    private Scanner scanner;
    private ExecutorService executorService;

    public Console(Service service, ExecutorService executorService) {
        this.service = service;
        this.scanner = new Scanner(System.in);
        this.executorService = executorService;
    }
    public void runConsole() {
        String cmd;
        boolean ok=true;
        while(ok){
            menu();
            System.out.print("Enter a command: ");
            cmd = scanner.next();
            switch (cmd) {
                case "1":
                    MovieCRUDConsole movieCRUDConsole = new MovieCRUDConsole(service, scanner, executorService);
                    movieCRUDConsole.runConsole();
                    break;
                case "2":
                    ClientCRUDConsole clientCRUDConsole = new ClientCRUDConsole(service, scanner, executorService);
                    clientCRUDConsole.runConsole();
                    break;
                case "3":
                    RentCRUDConsole rentCRUDConsole = new RentCRUDConsole(service, scanner, executorService);
                    rentCRUDConsole.runConsole();
                    break;
                case "0":
                    scanner.close();
                    ok=false;
                default:
                    break;
            }
        }
    }

    private void menu(){
        System.out.println("1. Movies");
        System.out.println("2. Clients");
        System.out.println("3. Rentals");
        System.out.println("0. Exit.\n");
    }
}
