import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class TcpClient {
    private static String serverHost;
    private static int serverPort;

    public TcpClient(String serverHost, int serverPort) {
        this.serverHost = serverHost;
        this.serverPort = serverPort;
    }

    public static <T> Message<T> sendAndReceive(Message<?> request){
        try(Socket socket = new Socket(serverHost,serverPort);
            OutputStream os = socket.getOutputStream();
            InputStream is = socket.getInputStream())
        {
            ObjectOutputStream oos = new ObjectOutputStream(os);
            ObjectInputStream ois = new ObjectInputStream(is);
            request.writeTo(oos);
            //System.out.println("Client sending"+request);

            Message<T> response = new Message<>();
            response.readFrom(ois);
            //System.out.println("Client received"+response);
            return response;
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
