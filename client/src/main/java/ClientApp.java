import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ClientApp {
    public static void main(String[] args){
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        TcpClient tcpClient = new TcpClient(Service.SERVER_HOST,Service.SERVER_PORT);
        Service serviceClient = new ClientService(executorService,tcpClient);

        Console console = new Console(serviceClient, executorService);
        console.runConsole();

        executorService.shutdownNow();
        System.out.println("Bye client!");
    }
}
