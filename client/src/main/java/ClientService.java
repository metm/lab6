import domain.Movie;
import domain.*;

import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class ClientService implements Service {
    private ExecutorService executorService;
    private TcpClient tcpClient;

    public ClientService(ExecutorService executorService, TcpClient tcpClient) {
        this.executorService = executorService;
        this.tcpClient = tcpClient;
    }

    @Override
    public Future<Message<String>> addMovie(Movie movie) {
        return executorService.submit(() -> {
            Message<Movie> request = new Message<>(Constants.AvailableMethods.addMovie.name(), movie);
            return TcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<Iterable<Movie>>> getAllMovies() {
        return executorService.submit(()->{
            Message<Void> request = new Message<>(Constants.AvailableMethods.getAllMovies.name());
            return tcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<String>> removeMovie(Integer id) {
        return executorService.submit(() -> {
            Message<Integer> request = new Message<>(Constants.AvailableMethods.removeMovie.name(), id);
            return TcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<Movie>> findMovie(Integer id) {
        return executorService.submit(()->{
            Message<Integer> request = new Message<>(Constants.AvailableMethods.findMovie.name(), id);
            return TcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<Iterable<Client>>> getAllClients() {
        return executorService.submit(()->{
            Message<Void> request = new Message<>(Constants.AvailableMethods.getAllClients.name());
            return tcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<String>> removeClient(Integer id) {
        return executorService.submit(() -> {
            Message<Integer> request = new Message<>(Constants.AvailableMethods.removeClient.name(), id);
            return TcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<Client>> findClient(Integer id) {
        return executorService.submit(()->{
            Message<Integer> request = new Message<>(Constants.AvailableMethods.findClient.name(), id);
            return TcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<Iterable<Rent>>> getAllRents() {
        return executorService.submit(()->{
            Message<Void> request = new Message<>(Constants.AvailableMethods.getAllRents.name());
            return tcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<Movie>> findMovieNameByID(Integer id) {
        return null;
    }

    @Override
    public CompletableFuture<Message<String>> addRent(Rent rent) {
        return CompletableFuture.supplyAsync(() -> {
            Message<Rent> request = new Message<>(Constants.AvailableMethods.addRent.name(), rent);
            return TcpClient.sendAndReceive(request);
        }, executorService);
    }

    @Override
    public Future<Message<String>> updateClient(Client newClient) {
        return executorService.submit(() -> {
            Message<Client> request = new Message<>(Constants.AvailableMethods.updateClient.name(), newClient);
            return TcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<String>> addClient(Client client) {
        return executorService.submit(() -> {
            Message<Client> request = new Message<>(Constants.AvailableMethods.addClient.name(), client);
            return TcpClient.sendAndReceive(request);
        });
    }

    @Override
    public Future<Message<String>> updateMovie(Movie newMovie) {
        return executorService.submit(() -> {
            Message<Movie> request = new Message<>(Constants.AvailableMethods.updateMovie.name(), newMovie);
            return TcpClient.sendAndReceive(request);
        });
    }
}
