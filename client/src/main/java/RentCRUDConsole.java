import domain.Client;
import domain.Rent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class RentCRUDConsole {
    private Service rentService;
    private Scanner scanner;
    private ExecutorService executorService;

    public RentCRUDConsole(Service rentService, Scanner scanner, ExecutorService executorService) {
        this.rentService = rentService;
        this.scanner = scanner;
        this.executorService = executorService;
    }

    public void runConsole() {
        String cmd;
        boolean ok=true;
        while(ok){
            menu();
            System.out.print("Enter a command: ");
            cmd = scanner.next();
            switch (cmd) {
                case "1":
                    addRent();
                    //this.executorService.submit(()->addMovie());
                    break;
                case "2":
                    printAllRents();
                    //this.executorService.submit(()->printAllMovies());
                    break;
                case "3":
                    //deleteClient();
                    //this.executorService.submit(() -> deleteMovie());
                    break;
                case "4":
                    //updateClient();
                    //this.executorService.submit(()->updateMovie());
                    break;
                case "0":
                    ok=false;
                    break;
                default:
                    break;
            }
        }
    }

    private static <T> T unpack(Future<Message<T>> msg){
        try {
            return msg.get().getBody();
        } catch (InterruptedException e) {
            System.out.println("An InterruptedException occured");
        } catch (ExecutionException e) {
            System.out.println("An ExecutionException occured");
        }
        return null;
    }

    private void printAllRents() {
        Iterable<Rent> rents = unpack(rentService.getAllRents());
        if (rents == null) return;
        rents.forEach(System.out::println);
        // try {
        //   movieService.getAllMovies().get().getBody().forEach(System.out::println);
        //} catch (InterruptedException | ExecutionException e) {
        //  e.printStackTrace();
        //}
    }

    private Rent readRent()
    {
        System.out.println("Read rent {id, movieID, clientID, startDate, dueDate}");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try{
            Integer id = Integer.valueOf(bufferRead.readLine());
            Integer movieID = Integer.valueOf(bufferRead.readLine());
            Integer clientID = Integer.valueOf(bufferRead.readLine());
            String startDate = bufferRead.readLine();
            String dueDate = bufferRead.readLine();


            SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
            java.util.Date parsed = format.parse(startDate);
            Date stDate = new Date(parsed.getTime());

            SimpleDateFormat format1 = new SimpleDateFormat("ddMMyyyy");
            java.util.Date parsed1 = format1.parse(dueDate);
            Date dDate = new Date(parsed1.getTime());

            Rent rent = new Rent(movieID, clientID, stDate, dDate);
            rent.setId(id);
            //movie.setId(id);

            return rent;
        } catch (IOException | ParseException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private void addRent()
    {
        Rent rent = readRent();
        System.out.println(unpack(rentService.addRent(rent)));
        /*if(!(client==null || client.getId()<0)){
            try{
                clientService.addClient(client);
            }catch (ValidatorException e){
                e.printStackTrace();
            }
        }
        else
        {
            System.out.println("Invalid id");
        }*/
    }

    private void menu(){
        System.out.println("1. Add a rental.");
        System.out.println("2. Print all rentals.");
        System.out.println("3. Delete a rental.");
        System.out.println("4. Update a rental.");
        System.out.println("0. Exit.");
    }
}
