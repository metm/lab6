import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;

import java.sql.Date;
import java.util.List;

public class SQLRentRepo implements SQLRepo<Rent> {
    @Autowired
    private JdbcOperations jdbcOperations;


    @Override
    public List<Rent> findAll() {
        String sql = "select * from rentals";
        return jdbcOperations.query(sql, (resultSet, rowNum) -> {

            int id = resultSet.getInt("rentid");
            int movieID = resultSet.getInt("mid");
            int clientID = resultSet.getInt("cid");
            Date startDate = resultSet.getDate("startDate");
            Date dueDate = resultSet.getDate("dueDate");

            Rent rt = new Rent(movieID, clientID, startDate, dueDate);
            rt.setId(id);
            return rt;
        });
    }

    @Override
    public void save(Rent entity) {
        String sql = "insert into rentals (rentid, mid, cid, startdate, duedate) values (?,?,?,?,?)";
        jdbcOperations.update(sql, entity.getId(), entity.getMovieID(), entity.getClientID(), entity.getStartDate(), entity.getDueDate());

    }

    @Override
    public void update(Rent entity) {

    }

    @Override
    public void deleteById(Integer id) {

    }
}
