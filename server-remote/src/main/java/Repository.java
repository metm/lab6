

import java.util.Optional;

/**
 * Interface for generic CRUD operations on a repository for a specific type.
 *
 * @author Diana
 *
 */

public interface Repository<ID, T extends BaseEntity<ID>> {

    /**
     * Adds the given entity.
     *
     * @param entity
     *            must not be null.
     * @return an {@code Optional} - null if the entity was saved(e.g. id already exists), otherwise returns the entity.
     * @throws IllegalArgumentException
     *             if the given entity is null.
     * @throws ValidatorException
     *             if the entity is not valid.
     */

    Optional<T> add(T entity) throws ValidatorException;

    /**
     *
     * @return all entities.
     */

    Iterable<T> getAll();

    /**
     * Removes the given entity.
     *
     * @param id
     *            must not be null.
     * @return an {@code Optional} - null if the entity doesn't exist, otherwise removes the entity with the given id.
     * @throws IllegalArgumentException
     *             if the given id is null.
     */
    Optional<T> remove(ID id) throws IllegalArgumentException;


    /**
     * Updates an entity with the given entity.
     *
     * @param newMovie
     *            must not be null.
     * @return an {@code Optional} - null if the entity doesn't exist, otherwise updates the entity with the given entity.
     * @throws ValidatorException
     *             if the given entity is null.
     */
    Optional<T> update(T newMovie) throws ValidatorException;
}
