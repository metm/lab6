import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;

import java.util.List;

public class SQLClientRepo implements SQLRepo<Client> {
    @Autowired
    private JdbcOperations jdbcOperations;

    @Override
    public List<Client> findAll() {
        String sql = "select * from clients";
        return jdbcOperations.query(sql, (rs, rowNum) -> {
            Integer id = rs.getInt("clientid");
            String firstname = rs.getString("firstname");
            String lastname = rs.getString("lastname");

            Client c = new Client(firstname, lastname);
            c.setId(id);

            return c;
        });
    }

    @Override
    public void save(Client entity) {
        String sql = "insert into clients (clientid, firstname, lastname) values (?,?,?)";
        jdbcOperations.update(sql, entity.getId(), entity.getfName(), entity.getlName());

    }

    @Override
    public void update(Client entity) {
        String sql = "update clients set firstname=?, lastname=? where clientid=?";
        jdbcOperations.update(sql, entity.getfName(), entity.getlName(),
                entity.getId());
    }

    @Override
    public void deleteById(Integer id) {
        String sql = "delete from clients where clientid=?";
        jdbcOperations.update(sql, id);
    }
}
