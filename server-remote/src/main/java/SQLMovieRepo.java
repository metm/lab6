import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;

import java.util.List;

public class SQLMovieRepo implements SQLRepo<Movie>{
    @Autowired
    private JdbcOperations jdbcOperations;

    @Override
    public List<Movie> findAll() {
        String sql = "select * from movies";
        return jdbcOperations.query(sql, (rs, rowNum) -> {
            Integer id = rs.getInt("movieid");
            String title = rs.getString("moviename");
            String description = rs.getString("description");
            String genre = rs.getString("genre");
            Movie m = new Movie(title, description, genre);
            m.setId(id);

            return m;
        });
    }

    @Override
    public void save(Movie movie) {
        String sql = "insert into movies (movieid, moviename, description, genre) values (?,?,?,?)";
        jdbcOperations.update(sql, movie.getId(), movie.getMovieName(), movie.getMovieDescription(), movie.getMovieGenre());
    }

    @Override
    public void update(Movie movie) {
        String sql = "update movies set moviename=?, description=?, genre=? where movieid=?";
        jdbcOperations.update(sql, movie.getMovieName(), movie.getMovieDescription(), movie.getMovieGenre(),
                movie.getId());
    }

    @Override
    public void deleteById(Integer id) {
        String sql = "delete from movies where movieid=?";
        jdbcOperations.update(sql, id);
    }
}
