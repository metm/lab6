import java.util.List;

public interface SQLRepo<T> {
    List<T> findAll();

    void save(T entity);

    void update(T entity);

    void deleteById(Integer id);
}
