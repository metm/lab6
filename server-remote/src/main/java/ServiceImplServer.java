public class ServiceImplServer implements Service {
    private SQLMovieRepo movieDBRepo;
    private SQLClientRepo clientDBRepo;
    private SQLRentRepo rentDBRepo;


    public ServiceImplServer(SQLMovieRepo movie, SQLClientRepo client, SQLRentRepo rent){
        this.movieDBRepo = movie;
        this. clientDBRepo = client;
        this.rentDBRepo = rent;
    }


    @Override
    public String addMovie(Movie movie) {
        try{
            movieDBRepo.save(movie);
            return "Success";}
        catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public Iterable<Movie> getAllMovies() {
        return movieDBRepo.findAll();

    }

    @Override
    public String removeMovie(Integer id) {
        try{
            movieDBRepo.deleteById(id);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public String updateMovie(Movie newMovie) {
        return null;
    }

    @Override
    public Movie findMovie(Integer id) {
        return null;
    }

    @Override
    public String addClient(Client client) {
        return null;
    }

    @Override
    public Iterable<Client> getAllClients() {
        return null;
    }

    @Override
    public String removeClient(Integer id) {
        return null;
    }

    @Override
    public String updateClient(Client newClient) {
        return null;
    }

    @Override
    public Client findClient(Integer id) {
        return null;
    }

    @Override
    public String addRent(Rent rent) {
        return null;
    }

    @Override
    public Iterable<Rent> getAllRents() {
        return null;
    }

    @Override
    public Movie findMovieNameByID(Integer id) {
        return null;
    }
}
