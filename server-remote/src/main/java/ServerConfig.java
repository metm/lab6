import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.remoting.rmi.RmiServiceExporter;
import org.apache.commons.dbcp2.BasicDataSource;

import javax.sql.DataSource;

@Configuration
public class ServerConfig {
    @Bean
    Service service(){
        return new ServiceImplServer(new SQLMovieRepo(), new SQLClientRepo(),
                new SQLRentRepo());
    }

    @Bean
    RmiServiceExporter rmiServiceExporter(){
        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceName("Service");
        exporter.setServiceInterface(Service.class);
        exporter.setService(this.service());
        return exporter;
    }

    @Bean
    JdbcOperations jdbcOperations() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();

        jdbcTemplate.setDataSource(dataSource());

        return jdbcTemplate;
    }

    @Bean
    DataSource dataSource() {
        BasicDataSource basicDataSource = new BasicDataSource();

        basicDataSource.setUrl("jdbc:postgresql://localhost:5432/MovieRentals");
        basicDataSource.setUsername("postgres");
        basicDataSource.setPassword("admin");
        basicDataSource.setInitialSize(2);

        return basicDataSource;
    }

}
